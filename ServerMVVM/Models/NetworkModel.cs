﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel;
using System.Xml;
using System.Collections.ObjectModel;

namespace ServerMVVM.Models
{
    class NetworkModel : INotifyPropertyChanged
    {
        // Starting end of the server application
        private string _ipAddress = "127.0.0.1"; // should make this immutable?
        public string IpAddress
        {
            get{return _ipAddress;}
            set{_ipAddress = value;OnPropertyChanged("IpAddress");}
        }

        private int _port = 8000;
        public int Port
        {
            get{return _port;}
            set{_port = value;OnPropertyChanged("Port");}
        }


        private int _connections = 0;
        public int Connections
        {
            get { return _connections; }
            set { _connections = value; OnPropertyChanged("Connections"); }
        }

        private bool _isRunning = false;
        public bool IsRunning
        {
            get { return _isRunning; }
            set
            {
                _isRunning = value;
                if (_isRunning == false)
                {
                    ServerButtonContent = "Start Server";
                    IsRunningColor = "#FE7F7D";
                }
                else
                {
                    ServerButtonContent = "Stop Server";
                    IsRunningColor = "#7DFF80";
                }
            }
        }

        private string _isRunningColor = "#FE7F7D";
        public string IsRunningColor
        {
            get
            {
                return _isRunningColor;
            }
            set
            {
                _isRunningColor = value;
                OnPropertyChanged("IsRunningColor");
            }
        }


        private string _serverButtonContent = "Start Server";
        public string ServerButtonContent
        {
            get
            {
                return _serverButtonContent;
            }

            set
            {
                _serverButtonContent = value;
                OnPropertyChanged("ServerButtonContent");
            }
        }


        private string _connectionLogString = "";
        public string ConnectionLogString
        {
            get { return _connectionLogString; }
            set { 
                _connectionLogString = value; 
                OnPropertyChanged("ConnectionLogString"); 
            }
        }

        //private ObservableCollection<string> _connectionCollection = new ObservableCollection<string>();
        //public ObservableCollection<string> ConnectionCollection{
        //    get { return _connectionCollection; }
        //    set { _connectionCollection = value; OnPropertyChanged("ConnectionCollection"); }
        //}
       

        

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if(handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
