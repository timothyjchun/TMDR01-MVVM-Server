﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace ServerMVVM.Models
{
    class DataModel : INotifyPropertyChanged
    {

        private bool _recieved = false;
        public bool Received
        {
            get { return _recieved; }
            set { _recieved = value; 
                if(_recieved == true){
                    ReceivedColor = "#56CE22";
                }
                else{
                    ReceivedColor = "#FE7F7D";
                }
                OnPropertyChanged("Received"); }
        }

        private string _receivedColor = "#FE7F7D";
        public string ReceivedColor
        {
            get { return _receivedColor; }
            set { _receivedColor = value; OnPropertyChanged("ReceivedColor");}
        }


        private bool _dataDecode = false;
        public bool DataDecode
        {
            get { return _dataDecode; }
            set { _dataDecode = value; 
                if(_dataDecode == true){
                    DataDecodeColor = "#56CE22";
                }
                else {
                    DataDecodeColor = "#FE7F7D";
                }
                OnPropertyChanged("DataDecode"); }
        }


        private string _dataDecodeColor = "#FE7F7D";
        public string DataDecodeColor
        {
            get { return _dataDecodeColor; }
            set { _dataDecodeColor = value; OnPropertyChanged("DataDecodeColor"); }
        }



        private string _dataDecodeResult = "";
        public string DataDecodeResult
        {
            get { return _dataDecodeResult; }
            set { _dataDecodeResult = value; OnPropertyChanged("DataDecodeResult"); }
        }


        private string _dataDecodeLog = "";
        public string DataDecodeLog
        {
            get { return _dataDecodeLog; }
            set { _dataDecodeLog = value; OnPropertyChanged("DataDecodeLog"); }
        }



        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if(handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
