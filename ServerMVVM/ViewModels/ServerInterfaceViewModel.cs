﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using ServerMVVM.Models;
using System.Runtime.Serialization;
using System.Diagnostics;

namespace ServerMVVM.ViewModels
{
    class ServerInterfaceViewModel : INotifyPropertyChanged
    {

        // models
        private Models.NetworkModel _networkModel;
        private Models.DataModel _dataModel;
        private Models.ConnectionModel _connectionModel;


        // commands
        public Commands.StartServerCommand StartServerCommand { get; set; }


        // threads
        private Threads.ServerThread ServerThread;
        public CancellationTokenSource cts = new CancellationTokenSource();


        public ServerInterfaceViewModel()
        {
            // models
            _networkModel = new Models.NetworkModel();
            _dataModel = new Models.DataModel();
            _connectionModel = new Models.ConnectionModel();


            // commands
            StartServerCommand = new Commands.StartServerCommand(StartServerExecuteFunc, StartServerCanExecuteFunc);


            // threads
            ServerThread = new Threads.ServerThread(false,"",0,cts, NetworkModel,DataModel);

        }

        public Models.NetworkModel NetworkModel
        {
            get { return _networkModel;}
            set { _networkModel = value; OnPropertyChanged("NetworkModel"); }
        }

        public Models.DataModel DataModel
        {
            get { return _dataModel;}
            set { _dataModel = value; OnPropertyChanged("DataModel"); }
        }

        public Models.ConnectionModel ConnectionModel
        {
            get { return _connectionModel; }
            set { _connectionModel = value; OnPropertyChanged("ConnectionModel"); }
        }


        private void StartServerExecuteFunc(object obj) {
            if(NetworkModel.IsRunning == false) // 새로운 서버 시작
            {
                NetworkModel.IsRunning = true;
                Debug.WriteLine("Preparing new server");
                string ipAddress = NetworkModel.IpAddress;
                int port = NetworkModel.Port;
                ServerThread.serverStatus = NetworkModel.IsRunning;
                ServerThread.IpAddress = ipAddress;
                ServerThread.Port = port;
                ServerThread.cancellationTokenSource = cts;
                ServerThread.startServerThread();
            }
            else if( NetworkModel.Connections==0 && NetworkModel.IsRunning == true ) // 이미 서버 있었음 => shut down 
            {
                ServerThread.stopServerThread();
                NetworkModel.Connections = 0;


                cts = new CancellationTokenSource();
                NetworkModel.IsRunning = false;
                Thread.Sleep(1500);
                Debug.WriteLine("Thread Ended");
                //ExecuteFunc(obj); // 재귀
            }
        }


        private bool StartServerCanExecuteFunc(object obj) {
            return true;
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
