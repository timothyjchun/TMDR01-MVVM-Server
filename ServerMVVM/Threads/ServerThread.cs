﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AwesomeSockets.Sockets;
using AwesomeSockets.Domain.Sockets;
using System.Diagnostics;
using System.Net.Sockets;
using Buffer = AwesomeSockets.Buffers.Buffer;
using System.Net;
using System.Windows.Documents;
using System.Reflection.Metadata.Ecma335;
using System.Security.Cryptography;



namespace ServerMVVM.Threads
{
    class ServerThread
    {
        
        public bool serverStatus;
        public string IpAddress;
        public int Port;
        public CancellationTokenSource cancellationTokenSource;

        private Thread? serverThread = null;
        private ISocket? listenSocket = null;

        private bool isClientConnected = false;

        // 이렇게 해도 되는지 모르겠지만 이렇게 하지 않으면 새로운 client가 접속하면 어떻게 반영해줘야하는지 모르겠다.
        private Models.NetworkModel networkModel;
        private Models.DataModel dataModel;

        public ServerThread(bool status,string ipAddress,int port, CancellationTokenSource cts, Models.NetworkModel NetworkModel, Models.DataModel DataModel)
        {
            serverStatus = status;
            IpAddress = ipAddress;
            Port = port;
            cancellationTokenSource = cts;


            networkModel = NetworkModel;
            dataModel = DataModel;
        }

        public void startServerThread()
        {
            Debug.WriteLine("starting thread");
            if (serverStatus == true)
            {
                serverThread = new Thread(() => serverListen(cancellationTokenSource.Token));
                serverThread.IsBackground = true;
                serverThread.Start();
            }
        }

        public void stopServerThread()
        {
            if (serverStatus == true)
            {
                serverStatus = false;
                listenSocket.Close(); // 그냥 여기서 listenSocket을 끊어버렸다.
                cancellationTokenSource.Cancel();
                serverThread.Join();
                cancellationTokenSource.Dispose();

                // reset NetworkModel Properties
                networkModel.Connections = 0;
                networkModel.IsRunning = false;

                // reset DataModel Properties
                dataModel.Received = false;
                dataModel.DataDecode = false;
                dataModel.DataDecodeResult = "";

                Debug.WriteLine(serverThread.IsAlive);
            }
        }

        private void serverListen(CancellationToken token) {
            while (!token.IsCancellationRequested)
            { 
                listenSocket = AweSock.TcpListen(Port);
                while (!token.IsCancellationRequested)
                {
                    try
                    {
                        Debug.WriteLine("Listening.....");
                        ISocket client = AweSock.TcpAccept(listenSocket); // Blocking Method로 돌아가기 때문에 여기서 멈춘다.
                        networkModel.Connections += 1;
                        isClientConnected = true;

                        while (isClientConnected == true)
                        { // 이렇게 하면 하나만 Accept 할 수 있다.
                            Buffer inBuff = Buffer.New();

                            bool logConnectionBool = true;
                            string endPoint = "";

                            while (checkSocketAlive(client.GetSocket()))
                            {
                                if (logConnectionBool == true) // 초기 connection 보여주기
                                {
                                    endPoint = IPAddress.Parse(((IPEndPoint)client.GetSocket().RemoteEndPoint).Address.ToString()).ToString();
                                    updateLogConnection(endPoint, "Connected");
                                    Debug.WriteLine(endPoint + "Connected");
                                    logConnectionBool = false;
                                }
                                Buffer.ClearBuffer(inBuff); // 소켓이 살아있으면 메시지 받기
                                Tuple<int, EndPoint> received = AweSock.ReceiveMessage(client, inBuff);
                                Thread.Sleep(50);
                                Buffer.FinalizeBuffer(inBuff);

                                if(checkLogOverflow(dataModel.DataDecodeLog, 50))
                                {
                                    dataModel.DataDecodeLog = "";
                                }
                                Tuple<bool, string> decodeResult = new Tuple<bool,string>(false,"");

                                if (received.Item1 != null && received.Item1 > 0)
                                {
                                    dataModel.Received = true; // 데이터 받기 성공

                                    byte[] receiveBuff = Buffer.GetBuffer(inBuff);
                                    decodeResult =  decodeData(receiveBuff); // 디코딩 시도
                                    if (decodeResult.Item1 == true)
                                    {
                                        dataModel.DataDecode = true; // 디코딩 성공
                                        updateDataDecodeResult(true, receiveBuff);
                                    }
                                    else
                                    {
                                        dataModel.DataDecode = false; // 디코딩 실패
                                        updateDataDecodeResult(false,null);
                                    }
                                }
                                else{
                                    dataModel.Received = false; // 데이터 받기 실패
                                    dataModel.DataDecode = false;
                                    updateDataDecodeResult(false, null);
                                }
                                updateDataDecodeLog(dataModel.Received, dataModel.DataDecode, decodeResult.Item2); // DataLog 업데이트 하기

                                Thread.Sleep(100);
                            }
                            updateLogConnection(endPoint, "Disconnected");
                            Debug.WriteLine("Disconnected");

                            networkModel.Connections -= 1;

                            // reset DataModel Properties when connections change
                            dataModel.Received = false;
                            dataModel.DataDecode = false;
                            dataModel.DataDecodeResult = "";
                            isClientConnected = false;
                            Thread.Sleep(100);
                        }

                        //if (isClientConnected == true) // start new thread that handles indivisual client
                        //{
                        //    Thread clientHandleThread = new Thread(() => clientHandle(client)); // problem->여러 개의 thread 어떻게 끌껀대?
                        //    clientHandleThread.IsBackground = true;
                        //    clientHandleThread.Start();
                        //}

                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine("TcpAccept failed. Closing Listening socket");
                        listenSocket.Close();
                        break;
                    }

                    Thread.Sleep(100);
                }
            }
            if(serverStatus == true) // 서버가 에러로 인해 꺼진거라면 Thread 까지 정리하도록 stopThread() 실행.
            {
                stopServerThread();
            }
        }

        private void updateDataDecodeLog(bool received, bool decode, string sData)
        {
            dataModel.DataDecodeLog = "----------------------------------------------------------------" + "\n"
                                      + "Time : " + getCurrentTimeString() + "\n"
                                      + "Received : " + received + "\n\n"
                                      + "Payload : " + sData + "\n"
                                      + "Decode Status : " + decode +"\n"
                                      + dataModel.DataDecodeLog;
        }

        private void updateDataDecodeResult(bool success , byte[]? data)
        {
            string dataLog = "";
            if (success)
            {
                dataLog = "---------------------------------------------------------" + "\n"
                            + "Data1:" + BitConverter.ToInt16(data, 0) + "\n"
                            + "Data2:" + BitConverter.ToSingle(data, 4) + "\n"
                            + "Data3:" + BitConverter.ToInt16(data, 8) + "\n"
                            +
                          "---------------------------------------------------------";
            }
            dataModel.DataDecodeResult = dataLog;
        }


        private Tuple<bool, string> decodeData(byte[] buff)
        {
            string result = "";
            try
            {
                result += BitConverter.ToInt16(buff, 0);
                result += " ";
                result += BitConverter.ToSingle(buff, 4);
                result += " ";
                result += BitConverter.ToInt16(buff, 8);
                
                Debug.WriteLine("====[start result string]====");
                Debug.WriteLine(result);
                Debug.WriteLine("====[end result string]====");
                return new Tuple<bool,string>(true, result);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return new Tuple<bool, string>(false, "");
            }
        }


        private void updateLogConnection(string endPoint,string status)
        {
            string currentTime = getCurrentTimeString();
            string logMsg = "-----------------------------------------------" 
                                    + "\nDateTime : " 
                                    + currentTime.ToString() + "\n"
                                    + "IP : " + endPoint + "\n" 
                                    + "Status : " + status+"\n";
            try {
                // networkModel.ConnectionLogString + logMsg;
                if (checkLogOverflow(networkModel.ConnectionLogString + logMsg,30)) {
                    Debug.WriteLine("Refreshing Log String");
                    networkModel.ConnectionLogString = "";
                }
                networkModel.ConnectionLogString = logMsg + networkModel.ConnectionLogString;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                //Debug.WriteLine("Couldn't Add to connectionCollection");
                Debug.WriteLine("Couldn't write connection log");
            }
        }


        private string getCurrentTimeString()
        {
            TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById("Korea Standard Time");
            DateTime currentTimeInKST = TimeZoneInfo.ConvertTime(DateTime.Now, tzi);
            return currentTimeInKST.ToString();
        }

        private bool checkLogOverflow(string log, int limit)
        {
            List<string> splitList = new List<string>(log.Split('\n'));
            return splitList.Count > limit;
        }



        private bool checkSocketAlive(Socket s)
        {
            bool part1 = s.Poll(1000, SelectMode.SelectRead);
            bool part2 = (s.Available == 0);
            if (part1 && part2)
                return false;
            else
                return true;
        }


        private void clientHandle(ISocket client)
        {
            bool checkSocketAlive(Socket s)
            {
                bool part1 = s.Poll(1000, SelectMode.SelectRead);
                bool part2 = (s.Available == 0);
                if (part1 && part2)
                    return false;
                else
                    return true;
            }


            Buffer inBuf = Buffer.New();
            Buffer outBuf = Buffer.New();

            bool x = true;

            while (checkSocketAlive(client.GetSocket()))
            {
                if (x == true)
                {
                    Debug.WriteLine(client);
                    IPAddress endPoint = IPAddress.Parse(((IPEndPoint)client.GetSocket().RemoteEndPoint).Address.ToString());
                    Debug.WriteLine(endPoint);
                    x = false;
                }
                // 소켓이 살아있으면 메시지 받기
                Buffer.ClearBuffer(inBuf);
                Tuple<int, EndPoint> received = AweSock.ReceiveMessage(client, inBuf);
                Thread.Sleep(100);
            }
        }



    }
}
